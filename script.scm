;;-----------------------------------------------------------------------------
;; tcgpong
;;
;; Contributors:
;;    Tetsumi <tetsumi@vmail.me>
;;
;; Copyright (C) 2015 Tetsumi
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;-----------------------------------------------------------------------------


(define SCREEN-WIDTH  640.0)
(define SCREEN-HEIGHT 480.0)

;;; <allegro5/keycodes.h>
(define ALLEGRO-KEY-D 4)
(define ALLEGRO-KEY-E 5)

(define PLAYER-WIDTH    10.0)
(define PLAYER-HEIGHT   60.0)
(define PLAYER-MARGIN   20.0)
(define PLAYER-VELOCITY 10.0)
(define PLAYER-MAX-Y (- SCREEN-HEIGHT PLAYER-HEIGHT))
(define PLAYER-MIDDLE-X  (- (/ SCREEN-HEIGHT 2.0) (/ PLAYER-HEIGHT 2.0)))

(define BALL-WIDTH 10.0)
(define BALL-MAX-X (- SCREEN-WIDTH  BALL-WIDTH))
(define BALL-MAX-Y (- SCREEN-HEIGHT BALL-WIDTH))
(define BALL-MIDDLE-X  (- (/  SCREEN-WIDTH 2.0) (/ BALL-WIDTH 2.0)))
(define BALL-MIDDLE-Y  (- (/ SCREEN-HEIGHT 2.0) (/ BALL-WIDTH 2.0)))

(define (playerA-update p)
  (define (move f)
    (define y (Player-y p))
    (define newY (f y (* 200.0 timeDelta)))
    
    (Player-y! p (cond [(> y PLAYER-MAX-Y) PLAYER-MAX-Y]
		       [(< y 0.0) 0.0]
		       [else newY])))
  
  (cond
   [(key-down? ALLEGRO-KEY-D) (move +)]
   [(key-down? ALLEGRO-KEY-E) (move -)]))

(define (playerB-update p)
  (define (move f)
    (define y (Player-y p))
    (define newY (f y (* 200.0 timeDelta)))
    
    (Player-y! p (cond [(> y PLAYER-MAX-Y) PLAYER-MAX-Y]
		       [(< y 0.0) 0.0]
		       [else newY])))
  
  (cond
   [(< (Player-y p) (Ball-y ball)) (move +)]
   [(> (Player-y p) (Ball-y ball)) (move -)]))

(define (nothing o) #f)

(define playerA (Player-make PLAYER-MIDDLE-X 0 playerA-update))
(define playerB (Player-make PLAYER-MIDDLE-X 0 playerB-update))

(define (ball-update b)
  (define x (Ball-x b))
  (define y (Ball-y b))
  (define vX (Ball-vX b))
  (define vY (Ball-vY b))
  (define newX (+ x (* vX timeDelta)))
  (define newY (+ y (* vY timeDelta)))
    
  (cond
   [(or (> x BALL-MAX-X) (< x 0.0))
    (Ball-vX! b (- vX))
    (Ball-x! b BALL-MIDDLE-X)
    (Ball-y! b BALL-MIDDLE-Y)
    (let ((player (if (> x BALL-MAX-X) playerA playerB)))
      (Player-score! player (+ 1 (Player-score player))))]
   [else 
    (Ball-x! b newX)
    (Ball-y! b (cond [(> y BALL-MAX-Y)
		    (Ball-vY! b (- vY))
		    BALL-MAX-Y]
		   [(< y 0.0)
		    (Ball-vY! b (- vY))
		    0.0]
		   [else newY]))]))
   
(define (ball-collision b p)
  (Ball-vX! b (- (Ball-vX b)))
  (Ball-vY! b (- (* 6.66 (- (Ball-y b) (Player-y p))) 200.0)))
    

(define ball (Ball-make BALL-MIDDLE-X
			BALL-MIDDLE-Y
			100.0
			100.0
			ball-update
			ball-collision))

(define (initialize)
  (display "Hello from GNU Guile!")
  (newline))

(define (finalize)
  (display "Bye from GNU Guile!")
  (newline))

(define secsPerUnit (exact->inexact internal-time-units-per-second))
(define oldTime (get-internal-real-time))
(define timeDelta 0.0)

(define (updateTimeDelta)
  (define curTime (get-internal-real-time))
  (set! timeDelta (/ (- curTime oldTime) secsPerUnit))
  (set! oldTime curTime))

(define (update)
  (updateTimeDelta))


