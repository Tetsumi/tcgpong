/*
  tcgpong
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_acodec.h>
#include <libguile.h>

typedef struct
{
	R32 x;
	R32 y;
	R32 right;
	U32 score;
	SCM update;
} Player;

typedef struct
{
	R32 x;
	R32 y;
	R32 vX;
	R32 vY;
	SCM update;
	SCM collision;
} Ball;

DEFINE_TYPEDEFS(Player);
DEFINE_TYPEDEFS(Ball);
DEFINE_TYPEDEFS(ALLEGRO_DISPLAY);
DEFINE_TYPEDEFS(ALLEGRO_EVENT_QUEUE);
DEFINE_TYPEDEFS(ALLEGRO_EVENT);
DEFINE_TYPEDEFS(ALLEGRO_EVENT_SOURCE);
DEFINE_TYPEDEFS(ALLEGRO_COLOR);
DEFINE_TYPEDEFS(ALLEGRO_SAMPLE);
DEFINE_TYPEDEFS(ALLEGRO_FONT);

ALLEGRO_KEYBOARD_STATE g_kbdState;
P_ALLEGRO_DISPLAY      g_display;
P_ALLEGRO_EVENT_QUEUE  g_queue;
scm_t_bits             Player_tag;
scm_t_bits             Ball_tag;
SCM                    g_update;
P_Player               g_playerA;
SCM                    g_playerAScm;
P_Player               g_playerB;
SCM                    g_playerBScm;
P_Ball                 g_ball;
SCM                    g_ballScm;
R32                    Player_width;
R32                    Player_height;
R32                    Ball_width;
R32                    Player_margin;
S32                    g_screenWidth;
S32                    g_screenHeight;
P_ALLEGRO_SAMPLE       g_ballSoundBorder;
P_ALLEGRO_SAMPLE       g_ballSoundPlayer;
P_ALLEGRO_FONT         g_font;


C_ALLEGRO_COLOR COLOR_WHITE = {
	.r = 1.0f,
	.g = 1.0f,
	.b = 1.0f,
	.a = 1.0f
};

C_ALLEGRO_COLOR COLOR_BLACK = {
	.r = 0.0f,
	.g = 0.0f,
	.b = 0.0f,
	.a = 0.0f
};

Error initializeGuile   (Void);
Error initializeAllegro (Void);
Error initialize        (Void);
Void  finalize          (Void);
Void  update            (Void);
Void  draw              (Void);
Error loop              (Void);
SCM   Player_y          (SCM player);
SCM   Player_set_y      (SCM player, SCM y);
SCM   Player_score      (SCM player);
SCM   Player_set_score  (SCM player, SCM score);
S32   Player_print      (SCM player, SCM port, scm_print_state *pstate);
SCM   Player_make       (SCM y, SCM score, SCM update);
Error Player_initialize (Void);
SCM   Ball_y            (SCM ball);
SCM   Ball_set_y        (SCM ball, SCM y);
SCM   Ball_x            (SCM ball);
SCM   Ball_set_x        (SCM ball, SCM x);
SCM   Ball_vX           (SCM ball);
SCM   Ball_set_vX       (SCM ball, SCM vX);
SCM   Ball_vY           (SCM ball);
SCM   Ball_set_vY       (SCM ball, SCM vY);
SCM   Ball_make         (SCM x,
			 SCM y,
			 SCM vX,
			 SCM vY,
			 SCM update,
			 SCM collision);
S32   Ball_print        (SCM ball, SCM port, scm_print_state *pstate);
Error Ball_initialize   (Void);
SCM   key_down          (SCM keyCode);

SCM Player_y (SCM player)
{
	return scm_from_double(((CPC_Player)SCM_SMOB_DATA(player))->y);
}

SCM Player_set_y (SCM player, SCM y)
{
	((CP_Player)SCM_SMOB_DATA(player))->y = scm_to_double(y);
	return SCM_UNSPECIFIED;
}

SCM Player_score (SCM player)
{
	return scm_from_uint32(((CPC_Player)SCM_SMOB_DATA(player))->score);
}

SCM Player_set_score (SCM player, SCM score)
{
        ((CP_Player)SCM_SMOB_DATA(player))->score = scm_to_uint32(score);
	return SCM_UNSPECIFIED;
}

S32 Player_print (SCM player, SCM port, scm_print_state *pstate)
{
	CPC_Player p = (CPC_Player)SCM_SMOB_DATA(player);
	Char buffer[100];
	sprintf(buffer, "#<Player(%.3f, %u)>", p->y, p->score);
	scm_puts (buffer, port);
	return 1;
}

SCM Player_make (SCM y, SCM score, SCM update)
{
	CP_Player p = (CP_Player)scm_gc_malloc(sizeof(Player), "Player");
	
	p->y      = scm_to_double(y);
	p->score  = scm_to_uint32(score);
	p->update = update;
	
	return scm_new_smob(Player_tag, (scm_t_bits)p);
}

SCM Player_mark (SCM player)
{
	return ((CPC_Player)SCM_SMOB_DATA(player))->update;
}

Error Player_initialize (Void)
{
	Player_tag = scm_make_smob_type("Player", sizeof(Player));
	scm_set_smob_print(Player_tag, Player_print);
	scm_set_smob_mark(Player_tag, Player_mark);
	scm_c_define_gsubr("Player-make",   3, 0, 0, Player_make);
	scm_c_define_gsubr("Player-y",      1, 0, 0, Player_y);
	scm_c_define_gsubr("Player-y!",     2, 0, 0, Player_set_y);
	scm_c_define_gsubr("Player-score",  1, 0, 0, Player_score);
	scm_c_define_gsubr("Player-score!", 2, 0, 0, Player_set_score);
	return ERROR_NONE;
}
	
SCM Ball_y (SCM ball)
{
	return scm_from_double(((CPC_Ball)SCM_SMOB_DATA(ball))->y);
}

SCM Ball_set_y (SCM ball, SCM y)
{
	((CP_Ball)SCM_SMOB_DATA(ball))->y = scm_to_double(y);
	return SCM_UNSPECIFIED;
}

SCM Ball_x (SCM ball)
{
	return scm_from_double(((CPC_Ball)SCM_SMOB_DATA(ball))->x);
}

SCM Ball_set_x (SCM ball, SCM x)
{
	((CP_Ball)SCM_SMOB_DATA(ball))->x = scm_to_double(x);
	return SCM_UNSPECIFIED;
}

SCM Ball_vX (SCM ball)
{
	return scm_from_double(((CPC_Ball)SCM_SMOB_DATA(ball))->vX);
}

SCM Ball_set_vX (SCM ball, SCM vX)
{
	((CP_Ball)SCM_SMOB_DATA(ball))->vX = scm_to_double(vX);
	return SCM_UNSPECIFIED;
}

SCM Ball_vY (SCM ball)
{
	return scm_from_double(((CPC_Ball)SCM_SMOB_DATA(ball))->vY);
}

SCM Ball_set_vY (SCM ball, SCM vY)
{
	((CP_Ball)SCM_SMOB_DATA(ball))->vY = scm_to_double(vY);
	return SCM_UNSPECIFIED;
}

SCM Ball_make (SCM x, SCM y, SCM vX, SCM vY, SCM update, SCM collision)
{
	CP_Ball b = (CP_Ball)scm_gc_malloc(sizeof(Ball), "Ball");
	
	b->x = scm_to_double(x);
	b->y = scm_to_double(y);
	b->vX = scm_to_double(vX);
	b->vY = scm_to_double(vY);
	b->update = update;
	b->collision = collision;
	
	return scm_new_smob(Ball_tag, (scm_t_bits)b);
}

S32 Ball_print (SCM ball, SCM port, scm_print_state *pstate)
{
	CPC_Ball b = (CPC_Ball)SCM_SMOB_DATA(ball);
	Char buffer[100];
	sprintf(buffer,
		"#<Ball(%.3f, %.3f, %3.f, %3.f, %3.f)>",
		b->x,
		b->y,
		b->vX,
		b->vY);
	scm_puts (buffer, port);
	return 1;
}

SCM Ball_mark (SCM ball)
{
	CPC_Ball b = (CPC_Ball)SCM_SMOB_DATA(ball);

	scm_gc_mark(b->update);

	return b->collision;
}

Error Ball_initialize (Void)
{
	Ball_tag = scm_make_smob_type("Ball", sizeof(Ball));
	scm_set_smob_print(Ball_tag, Ball_print);
	scm_set_smob_mark(Ball_tag, Ball_mark);
	scm_c_define_gsubr("Ball-make",   6, 0, 0, Ball_make);
	scm_c_define_gsubr("Ball-x",      1, 0, 0, Ball_x);
	scm_c_define_gsubr("Ball-x!",     2, 0, 0, Ball_set_x);
	scm_c_define_gsubr("Ball-y",      1, 0, 0, Ball_y);
	scm_c_define_gsubr("Ball-y!",     2, 0, 0, Ball_set_y);
	scm_c_define_gsubr("Ball-vX",     1, 0, 0, Ball_vX);
	scm_c_define_gsubr("Ball-vX!",    2, 0, 0, Ball_set_vX);
	scm_c_define_gsubr("Ball-vY",     1, 0, 0, Ball_vY);
	scm_c_define_gsubr("Ball-vY!",    2, 0, 0, Ball_set_vY);
	return ERROR_NONE;
}

SCM key_down (SCM keyCode)
{
	if (al_key_down(&g_kbdState, scm_to_int32(keyCode)))
		return SCM_BOOL_T;
	return SCM_BOOL_F;
}

Error initializeGuile (Void)
{	
	scm_init_guile();
	Player_initialize();
	Ball_initialize();
	scm_c_define_gsubr("key-down?", 1, 0, 0, key_down);

	if (scm_is_false(scm_c_primitive_load("script.scm")))
	{
		P_ERROR("failed to load script.scm.");
		return ERROR_GUILE;
	}
	
	scm_call_0(scm_variable_ref(scm_c_lookup("initialize")));
	g_update = scm_variable_ref(scm_c_lookup("update"));

	g_ballScm          = scm_variable_ref(scm_c_lookup("ball"));
	g_playerAScm       = scm_variable_ref(scm_c_lookup("playerA"));
	g_playerBScm       = scm_variable_ref(scm_c_lookup("playerB"));
	
	SCM playerWidth    = scm_variable_ref(scm_c_lookup("PLAYER-WIDTH"));
	SCM playerHeight   = scm_variable_ref(scm_c_lookup("PLAYER-HEIGHT"));
	SCM playerMargin   = scm_variable_ref(scm_c_lookup("PLAYER-MARGIN"));
	SCM ballWidth      = scm_variable_ref(scm_c_lookup("BALL-WIDTH"));
	SCM width          = scm_variable_ref(scm_c_lookup("SCREEN-WIDTH"));
	SCM height         = scm_variable_ref(scm_c_lookup("SCREEN-HEIGHT"));
	
	g_ball          = (P_Ball)SCM_SMOB_DATA(g_ballScm);
	g_playerA       = (P_Player)SCM_SMOB_DATA(g_playerAScm);
	g_playerB       = (P_Player)SCM_SMOB_DATA(g_playerBScm);
	Player_width    = scm_to_double(playerWidth);
	Player_height   = scm_to_double(playerHeight);
	Player_margin   = scm_to_double(playerMargin);
	Ball_width      = scm_to_double(ballWidth);
	g_screenWidth   = scm_to_double(width);
	g_screenHeight  = scm_to_double(height);
	
	return ERROR_NONE;
}

Error initializeAllegro (Void)
{	
	if(!al_init())
	{
		P_ERROR("failed to initialize allegro.");
		return ERROR_ALLEGRO;
	}

	g_display = al_create_display(g_screenWidth, g_screenHeight);
	
	if(!g_display)
	{
		P_ERROR("failed to create display.");
		return ERROR_ALLEGRO;
	}

	al_set_window_title(g_display, "tcrpong");

	g_queue = al_create_event_queue();

	if (!g_queue)
	{
		P_ERROR("failed to create event queue.");
		return ERROR_ALLEGRO;
	}

	CP_ALLEGRO_EVENT_SOURCE des = al_get_display_event_source(g_display);

	al_register_event_source(g_queue, des);

	if (!al_install_keyboard())
	{
		P_ERROR("failed to install keyboard.");
		return ERROR_ALLEGRO;
	}

	if (!al_install_audio())
	{
		P_ERROR("failed to install audio.");
		return ERROR_ALLEGRO;
	}

	if(!al_init_acodec_addon())
	{
		P_ERROR("failed to initialize acodec.");
		return ERROR_ALLEGRO;
	}

	if (!al_reserve_samples(1))
	{
		P_ERROR("failed to reserve samples.");
		return ERROR_ALLEGRO;
	}

	al_init_font_addon();

	if (!al_init_ttf_addon())
	{
		P_ERROR("failed to initialize allegro-ttf.");
		return ERROR_ALLEGRO;
	}
	
	return ERROR_NONE;
}

Error initialize (Void)
{
	Error e = ERROR_NONE;
	
	if (ERROR_NONE != (e = initializeGuile()))
		return e;
	if (ERROR_NONE != (e = initializeAllegro()))
		return e;
	
	atexit(finalize);
	
	C_R32 mW = Player_margin + Player_width;
	
	g_playerA->x     = Player_margin;
	g_playerA->right = mW;
        g_playerB->x     = g_screenWidth - mW;
	g_playerB->right = g_screenWidth - Player_margin;

	g_ballSoundBorder = al_load_sample("media/ball1.wav");

	if (!g_ballSoundBorder)
	{
		P_ERROR("couldn't load ball1.wav.");
		return ERROR_FILE;
	}

	g_ballSoundPlayer = al_load_sample("./media/ball2.wav");

	if (!g_ballSoundPlayer)
	{
		P_ERROR("couldn't load ball2.wav.");
		return ERROR_FILE;
	}

	g_font = al_load_font("./media/Minecrafter_3.ttf", 12, 0);
	
	return ERROR_NONE;
}

Void finalize (Void)
{
	scm_call_0(scm_variable_ref(scm_c_lookup("finalize")));

	if (g_ballSoundBorder)
		al_destroy_sample(g_ballSoundBorder);
	if (g_ballSoundPlayer)
		al_destroy_sample(g_ballSoundPlayer);
	if (g_font)
		al_destroy_font(g_font);
	if (g_queue)
		al_destroy_event_queue(g_queue);
	if (g_display)		
		al_destroy_display(g_display);
}

inline Boolean collide (R32 x1, R32 y1, R32 w1, R32 h1,
			R32 x2, R32 y2, R32 w2, R32 h2)
{
	return !(x1 > (x2 + w2)
		 || (x1 + w1) < x2
		 || y1 > (y2 + h2)
		 || (y1 + h1) < y2);
}

Void update (Void)
{
	al_get_keyboard_state(&g_kbdState);
	scm_call_0(g_update);
	scm_call_1(g_playerA->update, g_playerAScm);
	scm_call_1(g_playerB->update, g_playerBScm);
	scm_call_1(g_ball->update, g_ballScm);

	if (collide(g_playerA->x, g_playerA->y, Player_width, Player_height,
		    g_ball->x, g_ball->y, Ball_width, Ball_width))
	{
		scm_call_2(g_ball->collision, g_ballScm, g_playerAScm);
		al_play_sample(g_ballSoundPlayer,
			       1.0,
			       0.0,
			       1.0,
			       ALLEGRO_PLAYMODE_ONCE,
			       NULL);
	}
	
	if (collide(g_playerB->x, g_playerB->y, Player_width, Player_height,
		    g_ball->x, g_ball->y, Ball_width, Ball_width))
	{
		scm_call_2(g_ball->collision, g_ballScm, g_playerBScm);
		al_play_sample(g_ballSoundBorder,
			       1.0,
			       0.0,
			       1.0,
			       ALLEGRO_PLAYMODE_ONCE,
			       NULL);
	}
}

Void draw (Void)
{
	al_clear_to_color(COLOR_BLACK);

	char buffer[30];

	sprintf(buffer,"%u", g_playerA->score);	
	al_draw_text(g_font,
		     al_map_rgb(255,255,255),
		     (g_screenWidth / 2.0f) - 50.0f,
		     g_screenHeight / 4.0f,
		     ALLEGRO_ALIGN_CENTRE,
		     buffer);
	sprintf(buffer,"%u", g_playerB->score);	
	al_draw_text(g_font,
		     al_map_rgb(255,255,255),
		     (g_screenWidth / 2.0f) + 50.0f,
		     g_screenHeight / 4.0f,
		     ALLEGRO_ALIGN_CENTRE,
		     buffer);
	al_draw_filled_rectangle(g_playerA->x,
				 g_playerA->y,
				 g_playerA->right,
				 g_playerA->y  + Player_height,
				 COLOR_WHITE);
	al_draw_filled_rectangle(g_playerB->x,
				 g_playerB->y,
				 g_playerB->right,
				 g_playerB->y + Player_height,
				 COLOR_WHITE);
	al_draw_filled_rectangle(g_ball->x,
				 g_ball->y,
				 g_ball->x + Ball_width,
				 g_ball->y + Ball_width,
				 COLOR_WHITE);
	al_flip_display();
}

Error loop (Void)
{

	ALLEGRO_EVENT event;
	Boolean r = true;
	
	while (r)
	{
		if (al_get_next_event(g_queue, &event))
		{
			switch (event.type)
			{
			case ALLEGRO_EVENT_DISPLAY_CLOSE:
				r = false;
				continue;
				break;
			}
		}

		update();
		draw();
	}

	
	return ERROR_NONE;
}


int main (int argc, char *argv[])
{
	if (   ERROR_NONE != initialize()
	    || ERROR_NONE != loop())
		return EXIT_FAILURE;
}
