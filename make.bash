#!/bin/bash

ALLEGRO="-lallegro -lallegro_main -lallegro_primitives -lallegro_audio -lallegro_acodec
 -lallegro_font -lallegro_ttf"
GUILE_FLAGS=$(pkg-config guile-2.0 --cflags)
GUILE_LIBS=$(pkg-config guile-2.0 --libs)
SOURCE_FILES="main.c"

gcc -std=gnu11 -O3 $GUILE_FLAGS $GUILE_LIBS $ALLEGRO $SOURCE_FILES
